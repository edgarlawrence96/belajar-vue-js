new Vue({
    el: '#exercise',
    data: {
        name: 'bob iger',
        age: '27',
        numbers: ['0', '1'],
        chooseNumber: '',
        showImage: '<img src="/smile_homelander.jpg">'
    },
     methods: {
         RandomNumber: function() {
            var chooseNumber = Math.floor(Math.random() * this.numbers.length);
            this.chooseNumber = this.numbers[chooseNumber];
         }
     }
});