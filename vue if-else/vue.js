new Vue({
    el:'#app',
    data: {
        show: true,
        ingredients: ['meat', 'fruit', 'cookies'],
        persons: [
            {name: 'max', age: 27, color: 'red'},
            {name: 'anna', age: 17, color: 'blue'}
        ]
    }
});