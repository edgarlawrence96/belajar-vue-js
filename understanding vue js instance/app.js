var data = {
  title: 'The VueJS Instance',
  showParagraph: false
}

/// component for render specific html element
Vue.component('hello', {
    template: '<h1> hello </h1>'
});

var vm1 = new Vue({
  data: data, 
  methods: {
    show: function() {
      this.showParagraph = true;
      this.updateTitle('The VueJS Instance (Updated)');
      this.$refs.myButton.innerText = 'Test';
    },
    updateTitle: function(title) {
      this.title = title;
    }
  },
  computed: {
    lowercaseTitle: function() {
      return this.title.toLowerCase();
    }
  },
  watch: {
    title: function(value) {
      alert('Title changed, new value: ' + value);
    }
  }
});

vm1.$mount('#app1');

console.log(vm1.$data === data);
vm1.$refs.heading.innerText = 'something else';

setTimeout(() => {
  vm1.title = 'change by timer';
  vm1.show();
}, 3000);

var vm2 = new Vue({
    el: '#app2',
    data: {
      title: 'second instance'
    },
    methods: {
      onChange: function() {
        vm1.title = 'changed';
      }
    }
})

var vm3 = new Vue ({
  el: '.hello',
  template: '<h1> hello there </h1>'
});

// render with vue js
//vm3.$mount('#app3');

//render using original javascript
///vm3.$mount();
///document.getElementById('app3').appendChild(vm3.$el);