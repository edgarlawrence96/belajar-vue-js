new Vue({
    el:'#app',
    data: {
        title: 'Hello World',
        link: 'http://google.com',
        finishedLink: '<a href="http://youtube.com"> youtube </a>',
        counter: 0,
        SecondCounter: 0,
        x: 0,
        y: 0,
        name: 'John'

    },
    computed: {
        output: function() {
            console.log('computed');
            return this.counter > 5 ? 'greater 5' : 'smaller than 5';
        }
    },
    methods: {
        sayHello: function() {
            //return 'Jembi';
            this.title = 'konhiciwa minasan';
            return this.title;
        },
        Increase: function(step, event) {
            this.counter =+ step;
        },
        updateCoordinates: function(event) {
            this.x = event.clientX;
            this.y = event.clienty;
        },
        dummy: function(event) {
            event.stopPropagation();
        },
        alertMe: function() {
            alert('Alert');
        },
        result: function() {
            console.log('method');
            return this.counter > 5 ? 'greater 5' : 'smaller than 5';
        }
    }, watch: {
        counter: function(value) {
            var vm = this;
            setTimeout(function() {
                vm.counter = 0;
            }, 2000)
        }
    }
});
